# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/01/09 11:13:40 by mfamilar          #+#    #+#              #
#    Updated: 2016/02/24 11:39:04 by mfamilar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re norme

CXX = gcc

NAME = ft_ls

FLAGS = -Wall -Werror -Wextra

SRC_PATH = ./src/

OBJ_PATH = ./obj/

INC_PATH = ./include/

LIB_PATH = ./libft

SRC_NAME = main.c manage_list.c manage_list_2.c sort_argv.c search.c search_2.c search_3.c search_4.c flags.c recursive.c parse_tree.c parse_tree_2.c parse_tree_3.c insert_node.c affichage.c affichage_2.c affichage_3.c liste.c display_files.c flags_files.c display_files_2.c errors.c clean_argv.c search_5.c liste_2.c display_files_3.c display_files_4.c parse_tree_4.c insert_node_2.c initialize.c utilities.c sort_argv_2.c

OBJ_NAME = $(SRC_NAME:.c=.o)

LIB_NAME = -lft

SRC = $(addprefix $(SRC_PATH), $(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH), $(OBJ_NAME))
INC = $(addprefix -I, $(INC_PATH))
INC += $(addprefix -I, $(LIB_PATH))
LIB = $(addprefix -L, $(LIB_PATH))

all: $(NAME)

$(NAME): $(OBJ)
	MAKE -C ./libft
	$(CXX) $(FLAGS) $(LIB) $(LIB_NAME) $(OBJ) -o $(NAME) -g

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@mkdir $(OBJ_PATH) 2> /dev/null || echo "" > /dev/null
	$(CXX) $(FLAGS) $(INC) -c $< -o $@

clean:
	MAKE -C ./libft clean
	rm -fv $(OBJ)
	@rmdir $(OBJ_PATH) 2> /dev/null || echo "" > /dev/null

fclean: clean
	rm -rf ./libft/libft.a
	rm -fv $(NAME)

re: fclean all

norme:
	norminette $(SRC)
