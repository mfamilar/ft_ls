/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_files.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/29 14:24:47 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/23 14:03:29 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void			no_flags_file(t_liste *begin, t_obj *obj)
{
	while (begin)
	{
		obj->files = 1;
		ft_ls_no_flags_files(obj, begin->name);
		begin = begin->next;
	}
}

static void			lowr_flags_files(t_liste *begin, t_obj *obj)
{
	while (begin->next)
	{
		obj->files = 1;
		lowr_flag_files(obj, begin->name);
		begin = begin->next;
	}
}

static void			l_flags_files(t_liste *begin, t_obj *obj)
{
	while (begin)
	{
		obj->files = 1;
		ft_ls_l_one_dir_files(obj, begin->name);
		begin = begin->next;
	}
}

static void			lr_flags_files(t_liste *begin, t_obj *obj)
{
	while (begin->next)
	{
		obj->files = 1;
		ft_ls_l_one_dir_files(obj, begin->name);
		begin = begin->next;
	}
}

void				check_flags(t_liste *begin, t_obj *obj)
{
	if (!obj->list && !obj->time && !obj->reverse)
		no_flags_file(begin, obj);
	if (obj->reverse && !obj->list && !obj->time)
		lowr_flags_files(begin, obj);
	if (obj->list && !obj->reverse && !obj->time)
		l_flags_files(begin, obj);
	if (obj->time && !obj->list)
		t_flags_files(begin, obj);
	if (obj->list && obj->reverse && !obj->time)
		lr_flags_files(begin, obj);
	if (obj->list && obj->time)
		l_flags_files(begin, obj);
}
