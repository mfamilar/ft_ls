/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 19:07:57 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/24 11:07:59 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int			suite(t_obj *obj, int argc, char **argv)
{
	obj->argc = argc - 1;
	if (obj->time)
		if (sort_argv_by_time(argv, argc))
			return (1);
	if (obj->reverse)
	{
		argv = reverse_argv(argc, argv);
		if (!argv)
			return (1);
	}
	if (ft_ls_no_flags_multiple_dir(obj, argv, argc))
		return (1);
	return (0);
}

int					main(int argc, char **argv)
{
	t_obj *obj;

	if (!(obj = (t_obj*)malloc(sizeof(t_obj))))
		return (1);
	put_flags(argv, obj, argc);
	if (argc >= 2)
		display_files(argc, argv, obj);
	if (!(argv = clean_argv(argc, argv, obj)))
		return (1);
	argc = find_argc(argv);
	real_sort_argv(argv, argc);
	if (obj->list)
	{
		if (!(argv = check_dir_symbole(argc, argv, obj)))
			return (1);
		argc = find_argc(argv);
	}
	if (argc == 1 && !obj->nomore)
		if (no_flags_no_dir(obj))
			return (1);
	if (argc >= 2)
		if (suite(obj, argc, argv))
			return (1);
	return (0);
}
