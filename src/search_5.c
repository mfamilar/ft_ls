/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search_5.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 19:13:37 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/19 16:42:50 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int			search_max_lenght_links(t_tree *root)
{
	t_obj	*max;
	int		tmp;

	max = (t_obj*)malloc(sizeof(t_obj));
	if (!max)
		return (1);
	max->links = 0;
	tmp = find_max_links(root, max);
	put_max_lenght_links(root, ft_intlen(max->links));
	free(max);
	return (0);
}
