/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_tree.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 16:56:30 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/24 11:17:21 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void		parse_blk(t_tree *node, t_obj *obj)
{
	node->droits[0] = 'b';
	node->type = 4;
	obj->align = 1;
}

static void		parse_chr(t_tree *node, t_obj *obj)
{
	node->droits[0] = 'c';
	node->type = 5;
	obj->align = 1;
}

int				save_types(t_tree *node, struct stat st, t_obj *obj)
{
	node->droits = ft_memalloc(sizeof(char) * 11);
	if (!node->droits)
		return (1);
	if (S_ISDIR(st.st_mode))
	{
		node->droits[0] = 'd';
		node->type = 1;
	}
	if (S_ISLNK(st.st_mode))
	{
		node->droits[0] = 'l';
		node->type = 2;
	}
	if (S_ISBLK(st.st_mode))
		parse_blk(node, obj);
	if (S_ISCHR(st.st_mode))
		parse_chr(node, obj);
	save_types_2(node, st);
	if (save_rights(node, st))
		return (1);
	return (0);
}

void			manage_node(t_tree *root, t_tree *tmpnode,
		t_tree *node, t_tree **tree)
{
	node->left = NULL;
	node->right = NULL;
	if (root)
		insert_node(root, tmpnode, node);
	else
		*tree = node;
}

int				create_elem(t_tree **tree, struct dirent *dptr,
		struct stat st, t_obj *obj)
{
	t_tree	*node;
	t_tree	*tmpnode;
	t_tree	*root;

	if (obj->files)
		root = NULL;
	else
		root = *tree;
	tmpnode = NULL;
	if (!(node = (t_tree*)malloc(sizeof(t_tree))))
		return (1);
	initialize_tree_bol(node);
	if (create_elem_2(node, dptr, st, obj))
		return (1);
	if (!obj->reverse && !obj->time)
		manage_node(root, tmpnode, node, tree);
	if (obj->reverse && !obj->time)
		manage_reverse_node(root, tmpnode, node, tree);
	if (obj->time && !obj->reverse)
		manage_time_node(root, tmpnode, node, tree);
	if (obj->time && obj->reverse)
		manage_time_reverse_node(root, tmpnode, node, tree);
	return (0);
}
