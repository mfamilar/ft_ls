/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags_files.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/02 11:32:17 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/19 11:09:38 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int			ft_ls_no_flags_files(t_obj *obj, char *argv)
{
	DIR		*dp;

	obj->argc = 1;
	dp = opendir(argv);
	if (parse_list(dp, argv, obj))
		return (1);
	return (0);
}

int			ft_ls_l_one_dir_files(t_obj *obj, char *argv)
{
	DIR		*dp;

	obj->argc = 1;
	dp = opendir(argv);
	if (parse_list(dp, argv, obj))
		return (1);
	return (0);
}

int			lowr_flag_files(t_obj *obj, char *argv)
{
	if (ft_ls_no_flags_files(obj, argv))
		return (1);
	return (0);
}

int			ft_ls_t_flag_files(t_obj *obj, char *argv)
{
	DIR		*dp;

	obj->argc = 1;
	dp = opendir(argv);
	ft_ls_no_flags_files(obj, argv);
	return (0);
}
