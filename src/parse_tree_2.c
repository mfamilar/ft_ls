/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_tree_2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 17:13:01 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/24 11:18:41 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int	save_time(t_tree *node, struct stat st)
{
	char	*str;
	int		i;
	time_t	seconds;

	i = 4;
	if (!(node->time = ft_strdup(ctime(&st.st_mtime))))
		return (1);
	if (!(str = ft_strdup(ctime(&st.st_mtime))))
		return (1);
	node->nano = st.st_mtimespec.tv_nsec;
	node->ctime = st.st_mtime;
	seconds = time(NULL);
	if (seconds - node->six > node->ctime)
	{
		if (rec_year(node, str))
			return (1);
	}
	else
	{
		str[16] = 0;
		while (--i)
			str++;
		ft_strcpy(node->time, str);
	}
	return (0);
}

static int	save_names(t_tree *node, struct stat st)
{
	struct passwd	*pw;
	struct group	*gr;

	if ((gr = getgrgid(st.st_gid)) == NULL)
	{
		perror("");
		return (1);
	}
	else
		node->group_name = ft_strdup(gr->gr_name);
	if ((pw = getpwuid(st.st_uid)) == NULL)
	{
		perror("");
		return (1);
	}
	else
		node->user_name = ft_strdup(pw->pw_name);
	if (save_time(node, st))
		return (1);
	return (0);
}

static void	save_rights_3(t_tree *node, struct stat st)
{
	if (st.st_mode & S_IWOTH)
		node->droits[8] = 'w';
	else
		node->droits[8] = '-';
	if (st.st_mode & S_IXOTH)
	{
		node->droits[9] = 'x';
		if (st.st_mode & S_ISVTX)
			node->droits[9] = 't';
	}
	else if (st.st_mode & S_ISVTX)
		node->droits[9] = 'T';
	else
		node->droits[9] = '-';
	node->droits[10] = '\0';
	node->nb_links = st.st_nlink;
	if (node->type != 4 && node->type != 5)
		node->size = st.st_size;
	else
	{
		node->minor = minor(st.st_rdev);
		node->major = major(st.st_rdev);
	}
}

static int	save_rights_2(t_tree *node, struct stat st)
{
	if (st.st_mode & S_IRGRP)
		node->droits[4] = 'r';
	else
		node->droits[4] = '-';
	if (st.st_mode & S_IXGRP)
	{
		node->droits[6] = 'x';
		if (st.st_mode & S_ISGID)
			node->droits[6] = 's';
	}
	else if (st.st_mode & S_ISGID)
		node->droits[6] = 'S';
	else
		node->droits[6] = '-';
	save_rights_3(node, st);
	if (save_names(node, st))
		return (1);
	return (0);
}

int			save_rights(t_tree *node, struct stat st)
{
	if (st.st_mode & S_IRUSR)
		node->droits[1] = 'r';
	else
		node->droits[1] = '-';
	if (st.st_mode & S_IWUSR)
		node->droits[2] = 'w';
	else
		node->droits[2] = '-';
	if (st.st_mode & S_IXUSR)
	{
		node->droits[3] = 'x';
		if (st.st_mode & S_ISUID)
			node->droits[3] = 's';
	}
	else if (st.st_mode & S_ISUID)
		node->droits[3] = 'S';
	else
		node->droits[3] = '-';
	if (save_rights_2(node, st))
		return (1);
	return (0);
}
