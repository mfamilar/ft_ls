/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   affichage.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 13:47:41 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/24 11:07:47 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static	void		print_ls_4(t_tree *root, t_obj *obj)
{
	int		ret;

	ret = 0;
	if ((obj->list && obj->rec) || (obj->list && obj->cache))
		ft_putchar(' ');
	if (obj->files)
		ft_putchar(' ');
	ft_putnbr(root->nb_links);
	ft_putstr(" ");
	ret = root->max_lenght_user - ft_strlen(root->user_name);
	ft_putstr(root->user_name);
	while (ret--)
		ft_putchar(' ');
	ft_putstr("  ");
	ret = root->max_lenght_group - ft_strlen(root->group_name);
	ft_putstr(root->group_name);
	while (ret--)
		ft_putchar(' ');
	ft_putstr("  ");
}

void				print_ls_3(t_tree *root, t_obj *obj)
{
	if (obj->list)
	{
		print_ls_4(root, obj);
		if (root->type != 4 && root->type != 5)
			print_ls_5(root, obj);
		else
			print_ls_6(root);
		ft_putstr(root->time);
		ft_putstr(" ");
	}
	ft_putstr(root->name);
	if (root->type == 2 && obj->list)
	{
		ft_putstr(" -> ");
		ft_putstr(root->target);
	}
	if (obj->rec && obj->begin && ft_strcmp(root->dir, obj->last))
		return ;
	ft_putchar('\n');
}

static void			print_ls_2(t_tree *root, t_obj *obj)
{
	int		ret;

	ret = 0;
	if (root->left)
		print_ls_2(root->left, obj);
	if (root->name[0] != '.' && !obj->cache)
		no_hide(root, obj);
	else if (obj->cache)
		hide(root, obj);
	if (root->right)
		print_ls_2(root->right, obj);
}

void				print_ls(t_tree *root, t_obj *obj)
{
	if (!obj->files && !obj->recursive_counter)
		obj->counter++;
	if (obj->argc >= 2 || (obj->argc == 1 && obj->nomore) || obj->counter >= 2)
	{
		ft_putstr(root->dir);
		ft_putstr(":\n");
	}
	if (obj->list && !obj->files)
	{
		if (!(obj->rec && obj->total_size == 0 &&
					!search_empty(root) && !obj->cache))
		{
			ft_putstr("total ");
			ft_putnbr(obj->total_size);
			ft_putstr("\n");
		}
	}
	print_ls_2(root, obj);
}
