/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search_2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 17:32:05 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/23 13:38:56 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void				put_max_lenght_links(t_tree *root, int max)
{
	if (root->left)
		put_max_lenght_links(root->left, max);
	root->max_lenght_links = max;
	if (root->right)
		put_max_lenght_links(root->right, max);
}

static int			find_max_user(t_tree *root, t_obj *max)
{
	int		tmp;

	tmp = 0;
	if (root->left)
		find_max_user(root->left, max);
	if (root->right)
		find_max_user(root->right, max);
	tmp = ft_strlen(root->user_name);
	if (tmp > max->user)
		max->user = tmp;
	return (max->user);
}

static void			put_max_lenght_user(t_tree *root, int max)
{
	if (root->left)
		put_max_lenght_user(root->left, max);
	root->max_lenght_user = max;
	if (root->right)
		put_max_lenght_user(root->right, max);
}

int					search_max_lenght_user(t_tree *root)
{
	t_obj	*max;
	int		tmp;

	tmp = 0;
	max = (t_obj*)malloc(sizeof(t_obj));
	if (!max)
		return (1);
	max->user = 0;
	tmp = find_max_user(root, max);
	put_max_lenght_user(root, max->user);
	free(max);
	return (0);
}
