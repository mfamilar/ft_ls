/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initialize.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/09 14:12:05 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/19 10:10:03 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void			initialize_tree_bol(t_tree *node)
{
	node->nb_links = 0;
	node->size = 0;
	node->minor = 0;
	node->major = 0;
	node->max_lenght = 0;
	node->max_lenght_links = 0;
	node->max_lenght_user = 0;
	node->max_lenght_major = 0;
	node->max_lenght_minor = 0;
	node->ctime = 0;
	node->type = 0;
	node->total = 0;
	node->six = 15552000;
	node->time = 0;
}

static void		initalize_obj(t_obj *obj)
{
	obj->links = 0;
	obj->size = 0;
	obj->files = 0;
	obj->i = 0;
	obj->user = 0;
	obj->group = 0;
	obj->major = 0;
	obj->align = 0;
	obj->minor = 0;
	obj->total_size = 0;
	obj->list = 0;
	obj->rec = 0;
	obj->reverse = 0;
	obj->cache = 0;
	obj->time = 0;
	obj->error = 0;
	obj->argc = 0;
	obj->counter = 0;
	obj->begin = 0;
	obj->recursive_counter = 0;
	obj->dir = 0;
	obj->fake = 0;
	obj->nomore = 0;
	obj->first = 0;
}

void			put_flags(char **argv, t_obj *obj, int argc)
{
	int		i;

	i = 1;
	initalize_obj(obj);
	while (i < argc)
	{
		if (argv[i] && argv[i][0] == '-')
		{
			if (ft_strchr(argv[i], 'l'))
				obj->list = 1;
			if (ft_strchr(argv[i], 'R'))
				obj->rec = 1;
			if (ft_strchr(argv[i], 'r'))
				obj->reverse = 1;
			if (ft_strchr(argv[i], 't'))
				obj->time = 1;
			if (ft_strchr(argv[i], 'a'))
				obj->cache = 1;
		}
		i++;
	}
}
