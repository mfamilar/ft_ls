/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   insert_node.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 17:23:39 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/23 13:40:10 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void			insert_node(t_tree *root, t_tree *tmpnode, t_tree *node)
{
	int		i;

	while (root)
	{
		i = 0;
		tmpnode = root;
		while (root->name[i] == node->name[i])
			i++;
		if (node->name[i] > root->name[i])
		{
			root = root->right;
			if (!root)
				tmpnode->right = node;
		}
		else
		{
			root = root->left;
			if (!root)
				tmpnode->left = node;
		}
	}
}

void			insert_reverse_node(t_tree *root, t_tree *tmpnode, t_tree *node)
{
	int		i;

	while (root)
	{
		i = 0;
		tmpnode = root;
		while (root->name[i] == node->name[i])
			i++;
		if (node->name[i] > root->name[i])
		{
			root = root->left;
			if (!root)
				tmpnode->left = node;
		}
		else
		{
			root = root->right;
			if (!root)
				tmpnode->right = node;
		}
	}
}

static t_tree	*time_condition(t_tree *root, t_tree *tmpnode, t_tree *node)
{
	if (node->nano > root->nano)
	{
		root = root->left;
		if (!root)
			tmpnode->left = node;
	}
	else if (node->nano == root->nano && ft_strcmp(node->name, root->name) < 0)
	{
		root = root->left;
		if (!root)
			tmpnode->left = node;
	}
	else
	{
		root = root->right;
		if (!root)
			tmpnode->right = node;
	}
	return (root);
}

void			insert_time_node(t_tree *root,
		t_tree *tmpnode, t_tree *node)
{
	while (root)
	{
		tmpnode = root;
		if (node->ctime > root->ctime || node->ctime < 0)
		{
			root = root->left;
			if (!root)
				tmpnode->left = node;
		}
		else if (node->ctime == root->ctime)
			root = time_condition(root, tmpnode, node);
		else
		{
			root = root->right;
			if (!root)
				tmpnode->right = node;
		}
	}
}

void			insert_time_reverse_node(t_tree *root,
		t_tree *tmpnode, t_tree *node)
{
	while (root)
	{
		tmpnode = root;
		if (node->ctime > root->ctime || node->ctime < 0)
		{
			root = root->right;
			if (!root)
				tmpnode->right = node;
		}
		else if (node->ctime == root->ctime)
			root = time_condition_reverse(root, tmpnode, node);
		else
		{
			root = root->left;
			if (!root)
				tmpnode->left = node;
		}
	}
}
