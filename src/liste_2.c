/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   liste_2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 12:43:41 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/23 15:48:44 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void	nano_condition(t_liste *lst)
{
	if (lst->nano < lst->next->nano)
		swap(lst);
	else if (lst->nano == lst->next->nano &&
			ft_strcmp(lst->name, lst->next->name) > 0)
		swap(lst);
}

void		time_sort_liste(t_liste *begin)
{
	int		modif;
	t_liste	*lst;

	modif = 1;
	while (modif == 1)
	{
		modif = 0;
		lst = begin;
		while (lst->next)
		{
			if (lst->time < lst->next->time)
			{
				swap(lst);
				modif = 1;
			}
			else if (lst->time == lst->next->time)
				nano_condition(lst);
			lst = lst->next;
		}
	}
}

static void	nano_reverse(t_liste *lst)
{
	if (lst->nano > lst->next->nano)
		swap(lst);
	else if (lst->nano == lst->next->nano &&
			ft_strcmp(lst->name, lst->next->name) > 0)
		swap(lst);
}

void		reverse_time_sort_liste(t_liste *begin)
{
	int		modif;
	t_liste	*lst;

	modif = 1;
	while (modif == 1)
	{
		modif = 0;
		lst = begin;
		while (lst->next)
		{
			if (lst->time > lst->next->time)
			{
				swap(lst);
				modif = 1;
			}
			else if (lst->time == lst->next->time)
				nano_reverse(lst);
			lst = lst->next;
		}
	}
}
