/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   insert_node_2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/16 15:05:07 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/17 10:52:48 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_tree	*time_condition_reverse(t_tree *root, t_tree *tmpnode, t_tree *node)
{
	if (node->nano > root->nano)
	{
		root = root->right;
		if (!root)
			tmpnode->right = node;
	}
	else if (node->nano == root->nano && ft_strcmp(node->name, root->name) < 0)
	{
		root = root->right;
		if (!root)
			tmpnode->right = node;
	}
	else
	{
		root = root->left;
		if (!root)
			tmpnode->left = node;
	}
	return (root);
}
