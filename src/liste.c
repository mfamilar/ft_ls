/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   liste.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 10:14:24 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/24 11:15:48 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_liste			*create_element(t_liste *element, char *argv)
{
	element = (t_liste*)malloc(sizeof(t_liste));
	if (!element)
		return (NULL);
	element->name = ft_memalloc(sizeof(char) * (ft_strlen(argv) + 1));
	element->time = 0;
	element->nano = 0;
	element->next = NULL;
	return (element);
}

void			swap(t_liste *lst)
{
	int		tmp;
	char	*tmp_name;

	tmp = lst->time;
	lst->time = lst->next->time;
	lst->next->time = tmp;
	tmp = lst->nano;
	lst->nano = lst->next->nano;
	lst->next->nano = tmp;
	tmp_name = lst->name;
	lst->name = lst->next->name;
	lst->next->name = tmp_name;
}

t_liste			*sort_liste(t_liste *lst, int (cmp)(const char *, const char *))
{
	int		modif;
	t_liste	*flst;

	flst = lst;
	modif = 1;
	while (modif == 1)
	{
		modif = 0;
		lst = flst;
		while (lst->next)
		{
			if (cmp(lst->name, lst->next->name) > 0)
			{
				swap(lst);
				modif = 1;
			}
			lst = lst->next;
		}
	}
	if (flst->next)
		flst = flst->next;
	return (flst);
}
