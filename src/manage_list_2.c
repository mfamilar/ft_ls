/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_list_2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/23 13:43:05 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/23 17:29:45 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int		copy_path(t_obj *obj, char *argv, struct dirent *dptr)
{
	char		*tmp;

	tmp = ft_strjoin(argv, "/");
	if (!tmp)
		return (1);
	obj->fullpath = ft_strjoin(tmp, dptr->d_name);
	if (!obj->fullpath)
		return (1);
	free(tmp);
	return (0);
}

static void		counter(t_obj *obj, struct dirent *dptr, struct stat st)
{
	if (dptr->d_name[0] != '.' && !obj->cache)
		obj->total_size += st.st_blocks;
	else if (obj->cache)
		obj->total_size += st.st_blocks;
}

int				parse_list_2(char *argv, t_obj *obj, t_tree **node)
{
	struct dirent	*dptr;
	struct stat		st;

	dptr = NULL;
	ft_memset(&st, 0, sizeof(struct stat));
	obj->fullpath = ft_strdup(argv);
	if (!obj->fullpath)
		return (1);
	if (lstat(obj->fullpath, &st) == -1)
	{
		perror("stat");
		return (1);
	}
	if (create_elem(node, dptr, st, obj))
		return (1);
	return (0);
}

int				parse_list_1(DIR *dp, char *argv, t_obj *obj, t_tree **node)
{
	struct dirent	*dptr;
	struct stat		st;

	dptr = NULL;
	ft_memset(&st, 0, sizeof(struct stat));
	while ((dptr = readdir(dp)) != NULL)
	{
		if (copy_path(obj, argv, dptr))
			return (1);
		if (lstat(obj->fullpath, &st) == -1)
		{
			perror("stat");
			return (1);
		}
		counter(obj, dptr, st);
		if (create_elem(node, dptr, st, obj))
			return (1);
	}
	closedir(dp);
	return (0);
}
