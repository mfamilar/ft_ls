/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 09:53:14 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/23 14:44:01 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int			no_flags_no_dir(t_obj *obj)
{
	DIR		*dp;

	obj->argc = 1;
	dp = opendir(".");
	if (parse_list(dp, ".", obj))
		return (1);
	return (0);
}

int			ft_ls_no_flags(t_obj *obj, char *argv)
{
	DIR		*dp;

	dp = opendir(argv);
	if (!dp)
		return (1);
	if (parse_list(dp, argv, obj))
		return (1);
	return (0);
}

int			ft_ls_no_flags_multiple_dir(t_obj *obj, char **argv, int argc)
{
	DIR		*dp;
	int		i;

	i = 1;
	while (i < argc)
	{
		dp = opendir(argv[i]);
		if (!dp)
			return (1);
		if (obj->rec && obj->counter != 0)
			ft_putchar('\n');
		if (parse_list(dp, argv[i], obj))
			return (1);
		i++;
	}
	return (0);
}
