/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_files_4.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 16:14:04 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/12 15:46:02 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void		afficher_erreur(char **argv, int j, int i)
{
	if (argv[j][i] != 'l' && argv[j][i] != 'a' &&
			argv[j][i] != 'R' && argv[j][i] != 'r' &&
			argv[j][i] != '1' && argv[j][i] != 't')
	{
		ft_putstr_fd("ft_ls: illegal option -- ", 2);
		ft_putchar_fd(argv[j][i], 2);
		ft_putstr_fd("\nusage: ft_ls [-Ralrt1] [file ...]\n", 2);
		exit(EXIT_FAILURE);
	}
}

void			check_real(char **argv, t_obj *obj)
{
	int		c[2];

	c[0] = 0;
	while (argv[++c[0]])
	{
		c[1] = 0;
		if (argv[c[0]][0] == '-' && argv[c[0]][1] != '\0')
		{
			while (argv[c[0]][++c[1]] != '\0')
			{
				if (!ft_strcmp(argv[c[0]], "--"))
				{
					obj->i = c[0] + 1;
					return ;
				}
				afficher_erreur(argv, c[0], c[1]);
			}
		}
		else
		{
			obj->i = c[0];
			return ;
		}
	}
	obj->i = c[0];
}

void			t_flags_files(t_liste *begin, t_obj *obj)
{
	while (begin)
	{
		obj->files = 1;
		ft_ls_t_flag_files(obj, begin->name);
		begin = begin->next;
	}
}
