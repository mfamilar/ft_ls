/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search_4.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/15 12:38:32 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/23 13:42:36 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int			find_max_minor(t_tree *root, t_obj *max)
{
	int		tmp;

	tmp = 0;
	if (root->left)
		find_max_minor(root->left, max);
	if (root->right)
		find_max_minor(root->right, max);
	tmp = ft_intlen(root->minor);
	if (tmp > max->minor)
		max->minor = tmp;
	return (max->minor);
}

static void			put_max_lenght_minor(t_tree *root, int max)
{
	if (root->left)
		put_max_lenght_minor(root->left, max);
	root->max_lenght_minor = max;
	if (root->right)
		put_max_lenght_minor(root->right, max);
}

int					search_max_lenght_minor(t_tree *root)
{
	t_obj	*max;
	int		tmp;

	max = (t_obj*)malloc(sizeof(t_obj));
	if (!max)
		return (1);
	max->minor = 0;
	tmp = find_max_minor(root, max);
	put_max_lenght_minor(root, max->minor);
	free(max);
	return (0);
}

int					search_max_lenght_major(t_tree *root)
{
	t_obj	*max;
	int		tmp;

	max = (t_obj*)malloc(sizeof(t_obj));
	if (!max)
		return (1);
	max->major = 0;
	tmp = find_max_major(root, max);
	put_max_lenght_major(root, max->major);
	free(max);
	return (0);
}

int					search_empty(t_tree *root)
{
	while (root)
	{
		if (root->left)
		{
			if (root->name[0] != '.')
				return (1);
			root = root->left;
		}
		if (root->right)
		{
			if (root->name[0] != '.')
				return (1);
			root = root->right;
		}
		else if (!root->right && !root->left)
		{
			if (root->name[0] != '.')
				return (1);
			else
				return (0);
		}
	}
	return (1);
}
