/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_tree_4.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 16:54:49 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/24 11:28:34 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int			rec_year_suite(t_tree *node, char *str, char *year)
{
	int		i;

	if (ft_strlen(year) == 6)
		year[ft_strlen(year) - 1] = '\0';
	else if (ft_strlen(year) == 7)
		year[ft_strlen(year) - 1] = '\0';
	else if (ft_strlen(year) == 8)
		year[ft_strlen(year) - 2] = '\0';
	i = 4;
	while (--i)
		str++;
	str[8] = 0;
	node->time = ft_strjoin(str, year);
	if (!node->time)
		return (1);
	return (0);
}

int					rec_year(t_tree *node, char *str)
{
	int		i;
	int		j;
	char	*year;

	if (!(year = ft_memalloc(sizeof(char) * (ft_strlen(str) + 1))))
		return (1);
	i = 19;
	j = 1;
	year[0] = ' ';
	while (str[i])
	{
		while (str[i] == ' ')
			i++;
		year[j] = str[i];
		j++;
		i++;
	}
	if (rec_year_suite(node, str, year))
		return (1);
	return (0);
}

void				save_types_2(t_tree *node, struct stat st)
{
	if (S_ISSOCK(st.st_mode))
		node->droits[0] = 's';
	if (S_ISREG(st.st_mode))
	{
		node->droits[0] = '-';
		node->type = 3;
	}
	if (st.st_mode & S_IWGRP)
		node->droits[5] = 'w';
	else
		node->droits[5] = '-';
	if (st.st_mode & S_IROTH)
		node->droits[7] = 'r';
	else
		node->droits[7] = '-';
}
