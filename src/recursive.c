/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   recursive.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 11:33:29 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/23 13:38:05 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void	print_path(t_tree *tree)
{
	ft_putstr(tree->fullpath);
	ft_putstr(":\n");
}

static void	display(t_obj *obj, t_tree *tree)
{
	if (obj->counter != 0)
		ft_putchar('\n');
	if (obj->argc == 1)
		print_path(tree);
}

void		print_rec(t_tree *tree, t_obj *obj)
{
	if (!tree)
		return ;
	if (tree->left)
		print_rec(tree->left, obj);
	if (tree->name[0] != '.' && tree->type == 1)
	{
		display(obj, tree);
		obj->recursive_counter = 1;
		ft_ls_no_flags(obj, tree->fullpath);
		obj->recursive_counter = 0;
	}
	else if (tree->name[0] == '.' && obj->cache && ft_strcmp(tree->name, ".")
			&& ft_strcmp(tree->name, "..") && tree->type == 1)
	{
		display(obj, tree);
		obj->recursive_counter = 1;
		ft_ls_no_flags(obj, tree->fullpath);
		obj->recursive_counter = 0;
	}
	if (tree->right)
		print_rec(tree->right, obj);
}
