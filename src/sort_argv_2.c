/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_argv_2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/23 15:59:35 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/24 11:06:17 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void		swap_argv(char *argv1, char *argv2)
{
	char	*tmp;

	tmp = ft_strdup(argv1);
	argv1 = ft_strdup(argv2);
	argv2 = ft_strdup(tmp);
}

char		**reverse_argv(int argc, char **argv)
{
	int		i;
	int		j;
	char	**new_argv;

	j = 1;
	i = argc - 1;
	new_argv = NULL;
	if (!(new_argv = alloue_memoire(new_argv, argc, argv)))
		return (NULL);
	while (i > 0)
	{
		new_argv[j] = ft_strdup(argv[i]);
		if (!new_argv[i])
			return (NULL);
		i--;
		j++;
	}
	new_argv[j] = 0;
	return (new_argv);
}

char		**check_dir_symbole(int argc, char **argv, t_obj *obj)
{
	int			i;
	int			j;
	char		**new_argv;
	struct stat	st;

	ft_memset(&st, 0, sizeof(struct stat));
	i = 1;
	j = 1;
	new_argv = NULL;
	if (!(new_argv = alloue_memoire(new_argv, argc, argv)))
		return (NULL);
	while (i < argc)
	{
		lstat(argv[i], &st);
		if (!S_ISLNK(st.st_mode))
		{
			if (!(new_argv[j++] = ft_strdup(argv[i])))
				return (NULL);
		}
		else
			obj->nomore = 1;
		i++;
	}
	new_argv[j] = 0;
	return (new_argv);
}
