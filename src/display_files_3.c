/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_files_3.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 15:56:07 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/24 11:07:22 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static t_liste	*check_symbolic(t_liste *element, int argc,
		char **argv, t_obj *obj)
{
	t_liste			*begin;
	struct stat		st;
	int				i;

	i = 1;
	ft_memset(&st, 0, sizeof(struct stat));
	begin = element;
	while (i < argc)
	{
		lstat(argv[i], &st);
		if (S_ISLNK(st.st_mode))
		{
			if (element)
				while (element->next)
					element = element->next;
			element = create_ele(element, argv[i]);
			if (!begin)
				begin = element;
			obj->i = i;
			if (!(element = swap_list(element, obj, argv)))
				return (NULL);
		}
		i++;
	}
	return (begin);
}

t_liste			*parse_argv(t_obj *obj, int argc, char **argv, t_liste *element)
{
	DIR				*dp;
	t_liste			*begin;

	begin = NULL;
	while (obj->i < argc)
	{
		dp = opendir(argv[obj->i]);
		if (dp)
			parse_obj(obj, argv[obj->i]);
		if (!dp && errno == 20)
		{
			element = create_ele(element, argv[obj->i]);
			if (!element)
				return (NULL);
			begin = create_begin(begin, element);
			element = swap_list(element, obj, argv);
			if (!element)
				return (NULL);
		}
		obj->i++;
	}
	return (begin);
}

static void		display_files_suite(t_obj *obj, t_liste *begin)
{
	if (!obj->reverse)
		begin = sort_liste(begin, &ft_strcmp);
	if (obj->time && !obj->reverse)
		time_sort_liste(begin);
	if (obj->time && obj->reverse)
	{
		reverse_time_sort_liste(begin);
		begin = begin->next;
	}
	if (obj->reverse && !obj->time)
		reverse_sort_liste(begin);
	if (begin->name[0] != 0)
		check_flags(begin, obj);
	if (obj->dir && obj->files)
		ft_putchar('\n');
	obj->files = 0;
	free(begin);
}

int				display_files(int argc, char **argv, t_obj *obj)
{
	t_liste	*element;
	t_liste	*begin;
	int		pos;

	element = NULL;
	check_real(argv, obj);
	pos = obj->i;
	if (check_errors(obj, argc, argv, element))
		return (1);
	obj->i = pos;
	element = NULL;
	begin = parse_argv(obj, argc, argv, element);
	if (obj->list)
		begin = check_symbolic(begin, argc, argv, obj);
	free(element);
	if (begin == NULL)
	{
		obj->files = 0;
		free(begin);
		return (1);
	}
	else
		display_files_suite(obj, begin);
	return (0);
}
