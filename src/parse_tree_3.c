/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_tree_3.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 17:32:53 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/24 11:28:05 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int				add_symlinks(t_tree *node, t_obj *obj)
{
	int		len;

	len = 0;
	if (node->type == 2)
	{
		node->target = ft_memalloc(sizeof(char) * 4097);
		if (!node->target)
			return (1);
		len = readlink(obj->fullpath, node->target, 4096);
		node->target[len] = '\0';
	}
	node->fullpath = ft_memalloc(sizeof(char) * (ft_strlen(obj->fullpath) + 1));
	if (!node->fullpath)
		return (1);
	ft_strcpy(node->fullpath, obj->fullpath);
	return (0);
}

void			manage_reverse_node(t_tree *root, t_tree *tmpnode,
		t_tree *node, t_tree **tree)
{
	node->left = NULL;
	node->right = NULL;
	if (root)
		insert_reverse_node(root, tmpnode, node);
	else
		*tree = node;
}

void			manage_time_node(t_tree *root, t_tree *tmpnode,
		t_tree *node, t_tree **tree)
{
	node->left = NULL;
	node->right = NULL;
	if (root)
		insert_time_node(root, tmpnode, node);
	else
		*tree = node;
}

void			manage_time_reverse_node(t_tree *root, t_tree *tmpnode,
		t_tree *node, t_tree **tree)
{
	node->left = NULL;
	node->right = NULL;
	if (root)
		insert_time_reverse_node(root, tmpnode, node);
	else
		*tree = node;
}

int				create_elem_2(t_tree *node, struct dirent *dptr,
		struct stat st, t_obj *obj)
{
	if (!obj->files)
	{
		if (!(node->name = ft_memalloc(sizeof(char) *
						(ft_strlen(dptr->d_name) + 1))))
			return (1);
		ft_strcpy(node->name, dptr->d_name);
	}
	if (obj->files)
	{
		if (!(node->name = ft_memalloc(sizeof(char) *
						(ft_strlen(obj->fullpath) + 1))))
			return (1);
		ft_strcpy(node->name, obj->fullpath);
	}
	if (!(node->dir = ft_memalloc(sizeof(char) * (ft_strlen(obj->argv) + 1))))
		return (1);
	ft_strcpy(node->dir, obj->argv);
	if (obj->list || obj->rec || obj->time)
	{
		if (save_types(node, st, obj) || add_symlinks(node, obj))
			return (1);
	}
	return (0);
}
