/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search_3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/14 16:47:33 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/19 16:42:35 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int			find_max_group(t_tree *root, t_obj *max)
{
	int		tmp;

	tmp = 0;
	if (root->left)
		find_max_group(root->left, max);
	if (root->right)
		find_max_group(root->right, max);
	tmp = ft_strlen(root->group_name);
	if (tmp > max->group)
		max->group = tmp;
	return (max->group);
}

static void			put_max_lenght_group(t_tree *root, int max)
{
	if (root->left)
		put_max_lenght_group(root->left, max);
	root->max_lenght_group = max;
	if (root->right)
		put_max_lenght_group(root->right, max);
}

int					search_max_lenght_group(t_tree *root)
{
	t_obj	*max;
	int		tmp;

	max = (t_obj*)malloc(sizeof(t_obj));
	if (!max)
		return (0);
	max->group = 0;
	tmp = find_max_group(root, max);
	put_max_lenght_group(root, max->group);
	free(max);
	return (0);
}

int					find_max_major(t_tree *root, t_obj *max)
{
	int		tmp;

	tmp = 0;
	if (root->left)
		find_max_major(root->left, max);
	if (root->right)
		find_max_major(root->right, max);
	tmp = ft_intlen(root->major);
	if (tmp > max->major)
		max->major = tmp;
	return (max->major);
}

void				put_max_lenght_major(t_tree *root, int max)
{
	if (root->left)
		put_max_lenght_major(root->left, max);
	root->max_lenght_major = max;
	if (root->right)
		put_max_lenght_major(root->right, max);
}
