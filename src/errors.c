/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/04 13:54:51 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/24 12:14:06 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void		print_errors(t_liste *begin, t_obj *obj)
{
	if (obj->fake)
	{
		ft_putstr_fd("ft_ls: fts_open: ", 2);
		perror("");
		exit(EXIT_FAILURE);
	}
	if (begin != NULL)
		begin = sort_liste(begin, &ft_strcmp);
	if (begin != NULL && begin->name[0] != 0)
	{
		while (begin)
		{
			ft_putstr_fd("ls: ", 2);
			ft_putstr_fd(begin->name, 2);
			ft_putstr_fd(": ", 2);
			if (errno == 20)
				errno = 2;
			perror("");
			begin = begin->next;
		}
	}
}

static void		check_errors_suite(char **argv, t_obj *obj)
{
	if (argv[obj->i][0] == 0)
		obj->fake = 1;
}

int				check_errors(t_obj *obj, int argc,
		char **argv, t_liste *element)
{
	DIR		*dp;
	t_liste *begin;

	begin = NULL;
	while (obj->i < argc)
	{
		dp = opendir(argv[obj->i]);
		if (!dp && errno != 20)
		{
			if (argv[obj->i][1] != 0)
				check_errors_suite(argv, obj);
			element = create_ele(element, argv[obj->i]);
			if (!element)
				return (1);
			begin = create_begin(begin, element);
			element = swap_list(element, obj, argv);
			if (!element)
				return (1);
		}
		obj->i++;
	}
	print_errors(begin, obj);
	free(begin);
	return (0);
}
