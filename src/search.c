/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 17:30:36 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/23 13:37:19 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int			find_max_hide(t_tree *root, t_obj *max)
{
	int		tmp;

	tmp = 0;
	if (root->left)
		find_max_hide(root->left, max);
	if (root->right)
		find_max_hide(root->right, max);
	tmp = root->size;
	if (tmp > max->size)
		max->size = tmp;
	return (max->size);
}

static int			find_max(t_tree *root, t_obj *max)
{
	int		tmp;

	tmp = 0;
	if (root->left)
		find_max(root->left, max);
	if (root->right)
		find_max(root->right, max);
	tmp = root->size;
	if (tmp > max->size && root->name[0] != '.')
		max->size = tmp;
	return (max->size);
}

static void			put_max_lenght(t_tree *root, int max)
{
	if (root->left)
		put_max_lenght(root->left, max);
	root->max_lenght = max;
	if (root->right)
		put_max_lenght(root->right, max);
}

int					search_max_lenght_size(t_tree *root, t_obj *obj)
{
	t_obj	*max;
	int		tmp;

	tmp = 0;
	max = (t_obj*)malloc(sizeof(t_obj));
	if (!max)
		return (1);
	max->size = 0;
	if (!obj->cache)
		tmp = find_max(root, max);
	else if (obj->cache)
		tmp = find_max_hide(root, max);
	put_max_lenght(root, ft_intlen(max->size));
	free(max);
	return (0);
}

int					find_max_links(t_tree *root, t_obj *max)
{
	int		tmp;

	tmp = 0;
	if (root->left)
		find_max_links(root->left, max);
	if (root->right)
		find_max_links(root->right, max);
	tmp = root->nb_links;
	if (tmp > max->links)
		max->links = tmp;
	return (max->links);
}
