/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   affichage_5.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 11:50:21 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/19 09:09:03 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void			no_hide(t_tree *root, t_obj *obj)
{
	int		ret;

	ret = 0;
	if (obj->list)
	{
		ft_putstr(root->droits);
		ft_putstr(" ");
		ret = root->max_lenght_links - ft_intlen(root->nb_links);
		while (ret--)
			ft_putchar(' ');
	}
	print_ls_3(root, obj);
}

void			hide(t_tree *root, t_obj *obj)
{
	int		ret;

	ret = 0;
	if (obj->list)
	{
		ft_putstr(root->droits);
		ft_putstr(" ");
		ret = root->max_lenght_links - ft_intlen(root->nb_links);
		while (ret--)
			ft_putchar(' ');
	}
	print_ls_3(root, obj);
}
