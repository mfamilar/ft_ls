/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   affichage_2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 14:33:10 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/19 16:57:48 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void		print_ls_5(t_tree *root, t_obj *obj)
{
	int		ret;

	ret = 1;
	if (obj->align)
	{
		ret = root->max_lenght_major + root->max_lenght_minor - 2;
		while (ret--)
			ft_putchar(' ');
	}
	ret = root->max_lenght - ft_intlen(root->size);
	while (ret--)
		ft_putchar(' ');
	ft_putnbr(root->size);
}

void		print_ls_6(t_tree *root)
{
	int		ret;

	ret = 0;
	ret = root->max_lenght_major - ft_intlen(root->major);
	while (ret--)
		ft_putchar(' ');
	ft_putnbr(root->major);
	ft_putstr(", ");
	ret = root->max_lenght_minor - ft_intlen(root->minor);
	while (ret--)
		ft_putchar(' ');
	ft_putnbr(root->minor);
	ft_putstr(" ");
}
