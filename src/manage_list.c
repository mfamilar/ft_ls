/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_list.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 12:09:52 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/24 11:16:16 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int		ft_search(t_tree *node, t_obj *obj)
{
	if (search_max_lenght_size(node, obj))
		return (1);
	if (search_max_lenght_links(node))
		return (1);
	if (search_max_lenght_user(node))
		return (1);
	if (search_max_lenght_group(node))
		return (1);
	if (search_max_lenght_major(node))
		return (1);
	if (search_max_lenght_minor(node))
		return (1);
	return (0);
}

static int		parse_list_3(t_tree *node, t_obj *obj)
{
	if (obj->list)
		if (ft_search(node, obj))
			return (1);
	print_ls(node, obj);
	if ((obj->argc != obj->counter && !obj->rec) && !obj->files)
		ft_putchar('\n');
	if (obj->rec)
	{
		obj->total_size = 0;
		print_rec(node, obj);
	}
	obj->total_size = 0;
	return (0);
}

static int		copy(t_obj *obj, char *argv)
{
	obj->argv = ft_memalloc(sizeof(char) * (ft_strlen(argv) + 1));
	if (!obj->argv)
		return (1);
	ft_strcpy(obj->argv, argv);
	obj->fullpath = ft_memalloc(sizeof(char) * 4352);
	if (!obj->fullpath)
		return (1);
	return (0);
}

int				parse_list(DIR *dp, char *argv, t_obj *obj)
{
	struct dirent	*dptr;
	t_tree			*node;

	node = NULL;
	dptr = NULL;
	if (copy(obj, argv))
		return (1);
	if (!obj->files)
	{
		if (parse_list_1(dp, argv, obj, &node))
			return (1);
	}
	else
	{
		if (parse_list_2(argv, obj, &node))
			return (1);
	}
	if (parse_list_3(node, obj))
		return (1);
	free(node);
	return (0);
}
