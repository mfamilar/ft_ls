/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean_argv.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/05 12:05:50 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/24 11:09:31 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char		**alloue_memoire(char **new_argv, int argc, char **argv)
{
	int		i;

	i = 0;
	new_argv = ft_memalloc((sizeof(char*) * (argc + 1)));
	if (!new_argv)
		return (NULL);
	while (i < argc)
	{
		new_argv[i] = ft_strdup(argv[i]);
		if (!new_argv[i])
			return (NULL);
		i++;
	}
	new_argv[0] = ft_strdup(argv[0]);
	return (new_argv);
}

static void	print_erro(char **argv, t_obj *obj, int i)
{
	int		k;
	int		y;

	k = 1;
	y = i;
	if (!ft_strcmp(argv[y], "--"))
		obj->first++;
	while (argv[y][k] != '\0')
	{
		if (argv[y][k] != 'l' && argv[y][k] != 'a' &&
				argv[y][k] != 'R' &&
				argv[y][k] != 'r' && argv[y][k] != '1'
				&& argv[y][k] != 't' && ft_strcmp(argv[y], "--"))
			obj->nomore = 1;
		if (obj->first >= 2)
			obj->nomore = 1;
		k++;
	}
}

static void	clean_argv_suite(DIR *dp, char **argv, int i, t_obj *obj)
{
	if (!dp && argv[i][0] == '-' && argv[i][1] == '\0')
		obj->nomore = 1;
}

char		**clean_argv(int argc, char **argv, t_obj *obj)
{
	DIR		*dp;
	int		i[2];
	char	**new_argv;

	i[0] = 1;
	i[1] = 1;
	new_argv = NULL;
	if (!(new_argv = alloue_memoire(new_argv, argc, argv)))
		return (NULL);
	while (i[0] < argc)
	{
		dp = opendir(argv[i[0]]);
		if (dp)
			if (!(new_argv[i[1]++] = ft_strdup(argv[i[0]])))
				return (NULL);
		if (!dp && argv[i[0]][0] != '-')
			obj->nomore = 1;
		else if (!dp && argv[i[0]][0] == '-')
			print_erro(argv, obj, i[0]);
		clean_argv_suite(dp, argv, i[0], obj);
		i[0]++;
	}
	new_argv[i[1]] = 0;
	return (new_argv);
}

int			find_argc(char **argv)
{
	int		ac;
	int		i;

	i = 1;
	ac = 1;
	while (argv[i])
	{
		ac++;
		i++;
	}
	return (ac);
}
