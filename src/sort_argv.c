/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_argv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 12:13:12 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/23 17:31:29 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void						real_sort_argv(char **argv, int argc)
{
	int		i;
	int		j;
	char	*tmp;

	i = 0;
	j = 1;
	tmp = NULL;
	while (i < argc)
	{
		j = 1;
		while (j < argc - 1)
		{
			if (ft_strcmp(argv[j], argv[j + 1]) > 0)
			{
				tmp = ft_strdup(argv[j]);
				argv[j] = ft_strdup(argv[j + 1]);
				argv[j + 1] = ft_strdup(tmp);
			}
			j++;
		}
		i++;
	}
}

void						reverse_sort_liste(t_liste *begin)
{
	int		modif;
	t_liste	*lst;
	int		i;

	i = 0;
	modif = 1;
	while (modif == 1)
	{
		modif = 0;
		lst = begin;
		while (lst->next)
		{
			while (lst->name[i] == lst->next->name[i])
				i++;
			if (lst->name[i] < lst->next->name[i])
			{
				swap(lst);
				modif = 1;
			}
			i = 0;
			lst = lst->next;
		}
	}
}

static void					swap_argv(char **av, int i)
{
	char	*tmp;

	tmp = ft_strdup(av[i]);
	av[i] = ft_strdup(av[i + 1]);
	av[i + 1] = ft_strdup(tmp);
}

static int					nano_check(char **av, int i)
{
	struct stat		st;
	struct stat		st2;

	ft_memset(&st, 0, sizeof(struct stat));
	ft_memset(&st2, 0, sizeof(struct stat));
	if (lstat(av[i], &st) == -1 || lstat(av[i + 1], &st2) == -1)
		return (1);
	if (st.st_mtimespec.tv_nsec < st2.st_mtimespec.tv_nsec)
		swap_argv(av, i);
	else if (st.st_mtimespec.tv_nsec == st2.st_mtimespec.tv_nsec
			&& ft_strcmp(av[i], av[i + 1]) > 0)
		swap_argv(av, i);
	return (0);
}

int							sort_argv_by_time(char **av, int argc)
{
	int				i[2];
	struct stat		st;
	struct stat		st2;

	i[0] = 0;
	i[1] = 1;
	ft_memset(&st, 0, sizeof(struct stat));
	ft_memset(&st2, 0, sizeof(struct stat));
	while (i[0] < argc)
	{
		i[1] = 1;
		while (i[1] < argc - 1)
		{
			if (lstat(av[i[1]], &st) == -1 || lstat(av[i[1] + 1], &st2) == -1)
				return (1);
			if (st.st_mtime < st2.st_mtime)
				swap_argv(av, i[1]);
			else if (st.st_mtime == st2.st_mtime)
				if (nano_check(av, i[1]))
					return (1);
			i[1]++;
		}
		i[0]++;
	}
	return (0);
}
