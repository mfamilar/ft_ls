/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_files_2.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 14:35:47 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/23 17:28:42 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_liste			*create_begin(t_liste *begin, t_liste *element)
{
	if (!begin)
		begin = element;
	return (begin);
}

t_liste			*create_ele(t_liste *element, char *argv)
{
	if (!element)
		if (!(element = create_element(element, argv)))
			return (NULL);
	return (element);
}

void			parse_obj(t_obj *obj, char *argv)
{
	struct stat		st;

	ft_memset(&st, 0, sizeof(struct stat));
	lstat(argv, &st);
	if (S_ISLNK(st.st_mode) && obj->list)
		obj->dir = 0;
	else
		obj->dir = 1;
}

t_liste			*swap_list(t_liste *element, t_obj *obj, char **argv)
{
	struct stat		st;

	ft_memset(&st, 0, sizeof(struct stat));
	ft_strcpy(element->name, argv[obj->i]);
	lstat(element->name, &st);
	element->time = st.st_mtime;
	element->nano = st.st_mtimespec.tv_nsec;
	if (argv[obj->i + 1])
	{
		element->next = create_element(element->next, argv[obj->i + 1]);
		if (!element->next)
			return (NULL);
	}
	else
	{
		element->next = create_element(element->next, argv[obj->i]);
		if (!element->next)
			return (NULL);
	}
	element = element->next;
	return (element);
}
