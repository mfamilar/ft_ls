/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfamilar <mfamilar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/07 18:09:07 by mfamilar          #+#    #+#             */
/*   Updated: 2016/02/24 11:08:34 by mfamilar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H
# include <stdio.h>
# include <stdlib.h>
# include <dirent.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <unistd.h>
# include <pwd.h>
# include <uuid/uuid.h>
# include <grp.h>
# include <time.h>
# include <errno.h>
# include "libft.h"

typedef struct		s_tree
{
	int				six;
	int				nb_links;
	int				size;
	int				minor;
	int				major;
	int				max_lenght;
	int				max_lenght_links;
	int				max_lenght_user;
	int				max_lenght_group;
	int				max_lenght_major;
	int				max_lenght_minor;
	int				ctime;
	int				nano;
	int				type;
	int				total;
	char			*name;
	char			*fullpath;
	char			*target;
	char			*time;
	char			*user_name;
	char			*group_name;
	char			*dir;
	char			*droits;
	struct s_tree	*left;
	struct s_tree	*right;
}					t_tree;

typedef	struct		s_obj
{
	int				size;
	int				begin;
	int				files;
	int				argc;
	int				first;
	int				counter;
	int				dir;
	int				nomore;
	int				i;
	int				fake;
	int				links;
	int				user;
	int				group;
	int				time;
	int				error;
	int				major;
	int				align;
	int				minor;
	int				recursive_counter;
	int				total_size;
	int				list;
	int				reverse;
	int				cache;
	int				rec;
	char			*argv;
	char			*name;
	char			*fullpath;
	char			*last;
}					t_obj;

typedef struct		s_liste
{
	char			*name;
	int				nano;
	int				time;
	struct s_liste	*next;
}					t_liste;

void				copy_liste(t_tree *node, t_obj *obj);

void				insert_node(t_tree *root, t_tree *tmpnode, t_tree *node);
void				insert_reverse_node(t_tree *root,
		t_tree *tmpnode, t_tree *node);
void				insert_time_node(t_tree *root,
		t_tree *tmpnode, t_tree *node);
void				insert_time_reverse_node(t_tree *root,
		t_tree *tmpnode, t_tree *node);
void				print_ls(t_tree *root, t_obj *obj);
void				print_ls_5(t_tree *root, t_obj *obj);
void				print_ls_6(t_tree *root);

int					no_flags_no_dir(t_obj *obj);
int					ft_ls_no_flags(t_obj *obj, char *argv);
int					ft_ls_no_flags_files(t_obj *obj, char *argv);
int					ft_ls_no_flags_multiple_dir(t_obj *obj,
		char **argv, int argc);
int					ft_ls_l_one_dir_files(t_obj *obj, char *argv);
int					lowr_flag_files(t_obj *obj, char *argv);

int					parse_list(DIR *dp,
		char *argv, t_obj *obj);
int					parse_list_1(DIR *dp,
		char *argv, t_obj *obj, t_tree **node);

int					create_elem(t_tree **tree, struct dirent *dptr,
		struct stat st, t_obj *obj);
void				manage_node(t_tree *root, t_tree *tmpnode,
		t_tree *node, t_tree **tree);

void				manage_reverse_node(t_tree *root, t_tree *tmpnode,
		t_tree *node, t_tree **tree);

void				manage_time_node(t_tree *root, t_tree *tmpnode,
		t_tree *node, t_tree **tree);

void				manage_time_reverse_node(t_tree *root, t_tree *tmpnode,
		t_tree *node, t_tree **tree);
int					save_rights(t_tree *node, struct stat st);

int					add_symlinks(t_tree *node, t_obj *obj);

int					ft_intlen(int n);
void				put_max(t_tree *root, int max);
void				put_max_links(t_tree *root, int max);
void				put_max_lenght_links(t_tree *root, int max);
int					search_max_lenght_size(t_tree *root, t_obj *obj);
int					search_max_lenght_links(t_tree *root);
int					search_max_lenght_user(t_tree *root);
int					search_max_lenght_group(t_tree *root);
int					search_max_lenght_major(t_tree *root);
int					search_max_lenght_minor(t_tree *root);
int					find_max_major(t_tree *root, t_obj *obj);
void				put_max_lenght_major(t_tree *root, int max);

void				print_rec(t_tree *tree, t_obj *obj);
void				print_rec_hide(t_tree *tree, t_obj *obj);

void				l_flags_file(t_liste *begin, t_obj *obj);

void				print_ls_3(t_tree *root, t_obj *obj);
void				no_hide(t_tree *root, t_obj *obj);
void				hide(t_tree *root, t_obj *obj);

int					display_files(int argc, char **argv, t_obj *obj);

t_liste				*create_element(t_liste *element, char *argv);
void				reverse_sort_liste(t_liste *begin);
t_liste				*sort_liste(t_liste *begin,
					int (cmp)(const char *, const char *));

int					search_empty(t_tree *root);

void				put_flags(char **argv, t_obj *obj, int argc);

void				swap(t_liste *lst);

void				check_real(char **argv, t_obj *obj);
int					check_errors(t_obj *obj, int argc,
					char **argv, t_liste *element);
char				**clean_argv(int argc, char **argv, t_obj *obj);
void				real_sort_argv(char **argv, int argc);
int					find_argc(char **argv);
int					find_max_links(t_tree *root, t_obj *max);
void				initialize_tree_bol(t_tree *node);
int					ft_ls_t_flag_files(t_obj *obj, char *argv);
void				time_sort_liste(t_liste *begin);
void				reverse_time_sort_liste(t_liste *begin);
int					create_elem_2(t_tree *node, struct dirent *dptr,
					struct stat st, t_obj *obj);
int					save_types(t_tree *node, struct stat st, t_obj *obj);
int					parse_list_2(char *argv, t_obj *obj, t_tree **node);
t_liste				*swap_list(t_liste *element, t_obj *obj, char **argv);
t_liste				*create_ele(t_liste *element, char *argv);
t_liste				*create_begin(t_liste *begin, t_liste *element);
void				parse_obj(t_obj *obj, char *argv);
t_liste				*parse_argv(t_obj *obj, int argc, char **argv,
					t_liste *element);
void				check_flags(t_liste *begin, t_obj *obj);
int					rec_year(t_tree *node, char *str);
void				t_flags_files(t_liste *begin, t_obj *obj);
void				save_types_2(t_tree *node, struct stat st);
t_tree				*time_condition_reverse(t_tree *root, t_tree *tmpnode,
					t_tree *node);
int					sort_argv_by_time(char **argv, int argc);
char				**alloue_memoire(char **new_argv, int argc, char **argv);
char				**reverse_argv(int argc, char **argv);
char				**check_dir_symbole(int argc, char **argv, t_obj *obj);
#endif
